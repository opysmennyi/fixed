package creation_test;

import dao.constans_dao.ConstantDAO;
import dao.implementation.AgencyDaoImpl;
import model.entitydata.Agency;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.*;
import javax.sql.DataSource;
import org.mockito.Mock;
import persistant.persistant_implementation.ConnectionManager;
import view_controller.ConstantView.ConstantsView;
//import org.junit.Test;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

import static junit.framework.TestCase.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class AgencyTests {

    private ConnectionManager connectionManager;

    @Test
     void testGetConnection() throws SQLException {
        Connection con = ConnectionManager.getConnection();
        Assert.assertNotNull(con);
        Assert.assertTrue(con.isValid(1));
    }

//     @Mock
//     private DataSource ds;
//     @Mock
//     private Connection c;
//     @Mock
//     private PreparedStatement stmt;
//     @Mock
//     private ResultSet resultSet;
//     private Agency agency;
//     @Before
//     public void create() throws Exception {
//         assertNotNull(ds);
//         when(c.prepareStatement(any(String.class))).thenReturn(stmt);
//         when(ds.getConnection()).thenReturn(c);
//         agency = new Agency();
//         agency.setId_agency(1);
//         agency.setName_agency("Johannes");
//         agency.setCity("Dnipro");
//         when(resultSet.first()).thenReturn(true);
//         when(resultSet.getInt(1)).thenReturn(agency.getId_agency());
//         when(resultSet.getString(2)).thenReturn(agency.getName_agency());
//         when(resultSet.getString(3)).thenReturn(agency.getCity());
//         when(stmt.executeQuery()).thenReturn(resultSet);
//     }
//
//     @Test
//      void nullCreateThrowsException() throws SQLException {
//         new AgencyDaoImpl().create(null);
//     }
//     @Test
//      void createAgency() throws SQLException {
//         new AgencyDaoImpl().create(agency);
//     }
//@Mock
//private static PreparedStatement stmt;
//    @Before
//    public static Set<Integer> testfindAgencyAgentClientLandorInfo() throws SQLException {
//        assertNotNull(ConstantsView.AACL);
//        when(ConnectionManager.getConnection().prepareStatement(any(String.class))).thenReturn(stmt);
//        when(ConnectionManager.getConnection().prepareStatement(ConstantsView.AACL).getConnection());
//
//        Set<Integer> findAgencyAgentClientLandorInfo= new LinkedHashSet<>();
//        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(ConstantsView.AACL)) {
//            try (ResultSet resultSet = preparedStatement.executeQuery()) {
//                while (resultSet.next()) {
//                    System.out.println(("\nid_agency - " + resultSet.first()));
//                }
//            }
//            return findAgencyAgentClientLandorInfo;
//        }
//    }
//    @Test
//      void nullCreateThrowsException() throws SQLException {
//         new AgencyDaoImpl().create(null);
//     }

}
