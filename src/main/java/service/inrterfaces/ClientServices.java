package service.inrterfaces;

import model.entitydata.Client;

import java.sql.SQLException;

public interface ClientServices extends GeneralServices<Client, Integer> {
    int delete(Integer id_client) throws SQLException;
}
