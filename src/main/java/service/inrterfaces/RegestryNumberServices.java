package service.inrterfaces;

import model.entitydata.RegestryNumber;

import java.sql.SQLException;

public interface RegestryNumberServices extends GeneralServices<RegestryNumber, Integer> {
    int delete(Integer regestryNumber) throws SQLException;
}
