package service.inrterfaces;

import model.entitydata.Landlord;

import java.sql.SQLException;

public interface LandlordServices extends GeneralServices<Landlord, Integer> {
    int delete(Integer id_landlord) throws SQLException;
}
