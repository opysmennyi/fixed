package service.inrterfaces;

import model.entitydata.Agent_has_landlord;

import java.sql.SQLException;

public interface AgentHasLandorServices extends GeneralServices<Agent_has_landlord, Integer> {
    int delete(Integer id_agent) throws SQLException;
}
