package service.implementation;

import dao.constans_dao.ConstantDAO;
import dao.implementation.AgentDaoImpl;
import dao.interfaces.AgentDAO;
import model.entitydata.Agent;
import service.inrterfaces.AgentServices;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AgentServicesImpl implements AgentServices {

    @Override
    public List<Agent> findAll() throws SQLException {
        return new AgentDaoImpl().findAll();
    }

    @Override
    public Agent findById(Integer id_agent) throws SQLException {
        return new AgentDaoImpl().findById(id_agent);
    }

    @Override
    public int create(Agent agent) throws SQLException {
        return new AgentDaoImpl().create(agent);
    }

    @Override
    public int update(Agent agent) throws SQLException {
        return new AgentDaoImpl().update(agent);
    }

    @Override
    public int delete(Agent id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_agent) throws SQLException {
        return new AgentDaoImpl().delete(id_agent);
    }
}
