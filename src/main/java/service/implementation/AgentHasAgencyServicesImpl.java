package service.implementation;


import dao.implementation.AgentHasAgencyDaoImpl;
import model.entitydata.Agent_has_agency;
import service.inrterfaces.AgentHasAgencyServices;

import java.sql.SQLException;

import java.util.List;

public class AgentHasAgencyServicesImpl implements AgentHasAgencyServices {

    @Override
    public List<Agent_has_agency> findAll() throws SQLException {
        return new AgentHasAgencyDaoImpl().findAll();
    }

    @Override
    public Agent_has_agency findById(Integer id_agent) throws SQLException {
        return new AgentHasAgencyDaoImpl().findById(id_agent);
    }

    @Override
    public int create(Agent_has_agency agent_has_agency) throws SQLException {
        return new AgentHasAgencyDaoImpl().create(agent_has_agency);
    }

    @Override
    public int update(Agent_has_agency agent_has_agency) throws SQLException {
        return new AgentHasAgencyDaoImpl().update(agent_has_agency);
    }

    @Override
    public int delete(Agent_has_agency id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_agent) throws SQLException {
        return new AgentHasAgencyDaoImpl().delete(id_agent);
    }


}

