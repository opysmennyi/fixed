package service.implementation;

import dao.implementation.PlaceDaoImpl;
import model.entitydata.Place;
import service.inrterfaces.PlaceServices;

import java.sql.SQLException;
import java.util.List;

public class PlaceServicesImpl implements PlaceServices {

    @Override
    public List<Place> findAll() throws SQLException {
        return new PlaceDaoImpl().findAll();
    }

    @Override
    public Place findById(Integer id_place) throws SQLException {
        return new PlaceDaoImpl().findById(id_place);
    }

    @Override
    public int create(Place place) throws SQLException {
        return new PlaceDaoImpl().create(place);
    }

    @Override
    public int update(Place place) throws SQLException {
        return new PlaceDaoImpl().update(place);
    }

    @Override
    public int delete(Place id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id_place) throws SQLException {
        return new PlaceDaoImpl().delete(id_place);
    }
}
