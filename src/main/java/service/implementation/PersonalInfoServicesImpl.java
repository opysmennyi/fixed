package service.implementation;

import dao.implementation.PersonalInfoDaoImpl;
import model.entitydata.Personal_info;
import service.inrterfaces.PersonalInfoServices;

import java.sql.SQLException;
import java.util.List;

public class PersonalInfoServicesImpl implements PersonalInfoServices {
    @Override
    public List<Personal_info> findAll() throws SQLException {
        return new PersonalInfoDaoImpl().findAll();
    }

    @Override
    public Personal_info findById(Integer passport_id) throws SQLException {
        return new PersonalInfoDaoImpl().findById(passport_id);
    }

    @Override
    public int create(Personal_info personal_info) throws SQLException {
        return new PersonalInfoDaoImpl().create(personal_info);
    }

    @Override
    public int update(Personal_info personal_info) throws SQLException {
        return new PersonalInfoDaoImpl().update(personal_info);
    }

    @Override
    public int delete(Personal_info id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer passport_id) throws SQLException {
        return new PersonalInfoDaoImpl().delete(passport_id);
    }
}
