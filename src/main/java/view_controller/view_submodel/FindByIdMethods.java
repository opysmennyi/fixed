package view_controller.view_submodel;

import view_controller.ConstantView.ConstantsView;
import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class FindByIdMethods {
    private static Logger LOG = LogManager.getLogger(FindByIdMethods.class);

    public static void findByIdAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        Agency agency = agencyServices.findById(id_agency);
        LOG.info(agency);
    }

    public static void findByIdAgent() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgentServicesImpl agentServices = new AgentServicesImpl();
        Agent agent = agentServices.findById(id_agent);
        LOG.info(agent);
    }

    public static void findByIdAHA() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        Agent_has_agency agent_has_agency = agentHasAgencyServices.findById(id_agent);
        LOG.info(agent_has_agency);
    }

    public static void findByIdAHL() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        Agent_has_landlord agent_has_landlord = agentHasLandorServices.findById(id_agent);
        LOG.info(agent_has_landlord);
    }

    public static void findByIdClient() throws SQLException {
        LOG.trace(ConstantsView.INPUTCLIENTID);
        Integer id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        ClientServicesImpl clientServices = new ClientServicesImpl();
        Client client = clientServices.findById(id_client);
        LOG.info(client);
    }

    public static void findByIdLandlord() throws SQLException {
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        Integer id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LandorServicesImpl landorServices = new LandorServicesImpl();
        Landlord landlord = landorServices.findById(id_landor);
        LOG.info(landlord);
    }
    public static void findByIdPersonalInfo() throws SQLException {
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        Integer passport_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        Personal_info personal_info = personalInfoServices.findById(passport_id);
        LOG.info(personal_info);
    }
    public static void findByIdPlace() throws SQLException {
        LOG.trace(ConstantsView.INPUTPLACEID);
        Integer id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        Place place = placeServices.findById(id_place);
        LOG.info(place);
    }
    public static void findByIdRegestryNumber() throws SQLException {
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        Integer regestry_number = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        RegestryNumber regestryNumber = regestryNumberServices.findById(regestry_number);
        LOG.info(regestryNumber);
    }
}
