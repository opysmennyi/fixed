package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;
import service.inrterfaces.ClientServices;

import java.sql.SQLException;
import java.util.List;

public class SelectMethods {
    private static Logger LOG = LogManager.getLogger(SelectMethods.class);

    public static void selectAgency() throws SQLException {
        LOG.info("\nTableAnnotation: Agency");
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        List<Agency> agencies = agencyServices.findAll();
        for (Agency agency : agencies) {
            LOG.trace(agency);
        }
    }

    public static void selectAgent() throws SQLException {
        LOG.info("\nTableAnnotation: Agent");
        AgentServicesImpl agentServices = new AgentServicesImpl();
        List<Agent> agents = agentServices.findAll();
        for (Agent agent : agents) {
            LOG.trace(agent);
        }
    }


    public static void selectAHA() throws SQLException {
        LOG.info("\nTableAnnotation: Agent has agency");
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        List<Agent_has_agency> agent_has_agencies = agentHasAgencyServices.findAll();
        for (Agent_has_agency agent_has_agency : agent_has_agencies) {
            LOG.trace(agent_has_agency);
        }
    }

    public static void selectAHL() throws SQLException {
        LOG.info("\nTableAnnotation: Agent has landlord");
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        List<Agent_has_landlord> agent_has_landlords = agentHasLandorServices.findAll();
        for (Agent_has_landlord agent_has_landlord : agent_has_landlords) {
            LOG.trace(agent_has_landlord);
        }
    }

    public static void selectClient() throws SQLException {
        LOG.info("\nTableAnnotation: Client");
        ClientServicesImpl clientServices = new ClientServicesImpl();
        List<Client> clients = clientServices.findAll();
        for (Client client : clients) {
            LOG.trace(client);
        }
    }
    public static void selectLandlord() throws SQLException {
        LOG.info("\nTableAnnotation: Landlordr");
        LandorServicesImpl landorServices = new LandorServicesImpl();
        List<Landlord> landlords = landorServices.findAll();
        for (Landlord landlord : landlords) {
            LOG.trace(landlord);
        }
    }
    public static void selectPersonalInfo() throws SQLException {
        LOG.info("\nTableAnnotation: Personal info");
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        List<Personal_info> personal_infos = personalInfoServices.findAll();
        for (Personal_info personal_info : personal_infos) {
            LOG.trace(personal_info);
        }
    }
    public static void selectPlace() throws SQLException {
        LOG.info("\nTableAnnotation: Place");
        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        List<Place> places = placeServices.findAll();
        for (Place place : places) {
            LOG.trace(place);
        }
    }
    public static void selectRegestryNumber() throws SQLException {
        LOG.info("\nTableAnnotation: Registry number");
        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        List<RegestryNumber> regestryNumbers = regestryNumberServices.findAll();
        for (RegestryNumber regestryNumber : regestryNumbers) {
            LOG.trace(regestryNumber);
        }
    }
    public static void selectAllTable() throws SQLException {
        selectAgency();
        selectAgent();
        selectAHA();
        selectAHL();
        selectClient();
        selectLandlord();
        selectPersonalInfo();
        selectPlace();
        selectRegestryNumber();
    }
}
