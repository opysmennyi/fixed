package view_controller.view_submodel;

import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view_controller.ConstantView.ConstantsView;

public class CreateUpdateBase {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

    static Agency createUpdateAgency() {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        int id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTAGENCYNAME);
        String name = ConstantsView.INPUT.nextLine();
        LOG.trace("Input city of agency: ");
        String city = ConstantsView.INPUT.nextLine();
        LOG.trace("Input payment terms of agency: ");
        String payment_ters = ConstantsView.INPUT.nextLine();
        LOG.trace("Input agency murk-up: ");
        int markup = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input agency's services price: ");
        int agency_services_price = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input agreement termination for agency: ");
        String agreement_termination = ConstantsView.INPUT.nextLine();
        return new Agency(id_agency, name, city, payment_ters, markup, agency_services_price, agreement_termination);
    }

    static Agent createUpdateAgent() {
        LOG.trace(ConstantsView.INPUTAGENTID);
        int id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        return new Agent(id_agent, personal_info_passport_id);
    }

    static Agent_has_agency createUpdateAHA() {
        LOG.trace(ConstantsView.INPUTAGENTID);
        int id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTAGENCYID);
        int id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        return new Agent_has_agency(id_agent, id_agency);
    }

    static Agent_has_landlord createUpdateAHL() {
        LOG.trace(ConstantsView.INPUTAGENTID);
        int id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        int id_landlord = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        return new Agent_has_landlord(id_agent, id_landlord);
    }

    static Client createUpdateClient() {
        LOG.trace(ConstantsView.INPUTCLIENTID);
        int id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTAGENTID);
        int id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        return new Client(id_client, id_agent, personal_info_passport_id);
    }

    static Landlord createUpdatelandlord() {
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        int id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        String personal_info_passport_id = ConstantsView.INPUT.nextLine();
        return new Landlord(id_landor, personal_info_passport_id);
    }

    static Personal_info createUpdatePersonalInfo() {
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        String passport_id = ConstantsView.INPUT.nextLine();
        LOG.trace("Input name: ");
        String name = ConstantsView.INPUT.nextLine();
        LOG.trace("Input surname: ");
        String surname = ConstantsView.INPUT.nextLine();
        LOG.trace("Input age: ");
        int age = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input family status(married/single): ");
        String family_status = ConstantsView.INPUT.nextLine();
        return new Personal_info(passport_id, name, surname, age, family_status);
    }

     static Place createUpdatePlace() {
        LOG.trace(ConstantsView.INPUTPLACEID);
        int id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input square for place: ");
        int square = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input price for place: ");
        int price = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace("Input location of place: ");
        String location = ConstantsView.INPUT.nextLine();
        LOG.trace("Input type of place: ");
        String type = ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        int landor_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        String regestry_number = ConstantsView.INPUT.nextLine();
        return new Place(id_place, square, price, location, type, landor_id, regestry_number);
    }

     static RegestryNumber createUpdareRegestryNumber() {
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        String regestry_number = ConstantsView.INPUT.nextLine();
        return new RegestryNumber(regestry_number);
    }
}
