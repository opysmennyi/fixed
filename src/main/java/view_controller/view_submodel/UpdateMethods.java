package view_controller.view_submodel;

import view_controller.ConstantView.ConstantsView;
import model.entitydata.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class UpdateMethods {
    private static Logger LOG = LogManager.getLogger(UpdateMethods.class);

    public static void updateAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        int count = agencyServices.update(CreateUpdateBase.createUpdateAgency());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAgent() throws SQLException {
        AgentServicesImpl agentServices = new AgentServicesImpl();
        int count = agentServices.update(CreateUpdateBase.createUpdateAgent());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHA() throws SQLException {
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        int count = agentHasAgencyServices.update(CreateUpdateBase.createUpdateAHA());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateAHL() throws SQLException {
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        int count = agentHasLandorServices.update(CreateUpdateBase.createUpdateAHL());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void updateClient() throws SQLException {
        ClientServicesImpl clientServices = new ClientServicesImpl();
        int count = clientServices.update(CreateUpdateBase.createUpdateClient());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateLandlord() throws SQLException {
        LandorServicesImpl landorServices = new LandorServicesImpl();
        int count = landorServices.update(CreateUpdateBase.createUpdatelandlord());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateePersonalInfo() throws SQLException {
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        int count = personalInfoServices.update(CreateUpdateBase.createUpdatePersonalInfo());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updatePlace() throws SQLException {
        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        int count = placeServices.update(CreateUpdateBase.createUpdatePlace());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void updateRegestryNumber() throws SQLException {
        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        int count = regestryNumberServices.update(CreateUpdateBase.createUpdareRegestryNumber());
        LOG.info(ConstantsView.CREATED + count);
    }
}
