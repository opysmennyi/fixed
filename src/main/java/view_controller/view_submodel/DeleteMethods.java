package view_controller.view_submodel;

import view_controller.ConstantView.ConstantsView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class DeleteMethods {
    private static Logger LOG = LogManager.getLogger(DeleteMethods.class);

    public static void deleteAgency() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENCYID);
        Integer id_agency = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        int count = agencyServices.delete(id_agency);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAgent() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgentServicesImpl agentServices = new AgentServicesImpl();
        int count = agentServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAHA() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        int count = agentHasAgencyServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteAHL() throws SQLException {
        LOG.trace(ConstantsView.INPUTAGENTID);
        Integer id_agent = ConstantsView.INPUT.nextInt();
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        int count = agentHasLandorServices.delete(id_agent);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteClient() throws SQLException {
        LOG.trace(ConstantsView.INPUTCLIENTID);
        Integer id_client = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        ClientServicesImpl clientServices = new ClientServicesImpl();
        int count = clientServices.delete(id_client);
        LOG.info(ConstantsView.DELETED, count);
    }

    public static void deleteLandlord() throws SQLException {
        LOG.trace(ConstantsView.INPUTLANDLORDID);
        Integer id_landor = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        LandorServicesImpl landorServices = new LandorServicesImpl();
        int count = landorServices.delete(id_landor);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deletePersonalInfo() throws SQLException {
        LOG.trace(ConstantsView.INPUTPASSPORTID);
        Integer passport_id = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        int count = personalInfoServices.delete(passport_id);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deletePlace() throws SQLException {
        LOG.trace(ConstantsView.INPUTPLACEID);
        Integer id_place = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        int count = placeServices.delete(id_place);
        LOG.info(ConstantsView.DELETED, count);
    }
    public static void deleteRegestryNumber() throws SQLException {
        LOG.trace(ConstantsView.INPUTREGISTRYNUMBER);
        Integer regestry_number = ConstantsView.INPUT.nextInt();
        ConstantsView.INPUT.nextLine();
        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        int count = regestryNumberServices.delete(regestry_number);
        LOG.info(ConstantsView.DELETED, count);
    }
}
