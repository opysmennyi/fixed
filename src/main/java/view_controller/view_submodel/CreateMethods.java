package view_controller.view_submodel;

import view_controller.ConstantView.ConstantsView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.implementation.*;

import java.sql.SQLException;

public class CreateMethods {

    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

    public static void createAgency() throws SQLException {
        AgencyServicesImpl agencyServices = new AgencyServicesImpl();
        int count = agencyServices.create(CreateUpdateBase.createUpdateAgency());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createAgent() throws SQLException {
        AgentServicesImpl agentServices = new AgentServicesImpl();
        int count = agentServices.create(CreateUpdateBase.createUpdateAgent());
        LOG.info(ConstantsView.CREATED + count);

    }
    public static void createAHA() throws SQLException {
        AgentHasAgencyServicesImpl agentHasAgencyServices = new AgentHasAgencyServicesImpl();
        int count = agentHasAgencyServices.create(CreateUpdateBase.createUpdateAHA());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createAHL() throws SQLException {
        AgentHasLandorServicesImpl agentHasLandorServices = new AgentHasLandorServicesImpl();
        int count = agentHasLandorServices.create(CreateUpdateBase.createUpdateAHL());
        LOG.info(ConstantsView.CREATED + count);
    }

    public static void createClient() throws SQLException {

        ClientServicesImpl clientServices = new ClientServicesImpl();
        int count = clientServices.create(CreateUpdateBase.createUpdateClient());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createLandlord() throws SQLException {
        LandorServicesImpl landorServices = new LandorServicesImpl();
        int count = landorServices.create(CreateUpdateBase.createUpdatelandlord());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createPersonalInfo() throws SQLException {
        PersonalInfoServicesImpl personalInfoServices = new PersonalInfoServicesImpl();
        int count = personalInfoServices.create(CreateUpdateBase.createUpdatePersonalInfo());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createPlace() throws SQLException {

        PlaceServicesImpl placeServices = new PlaceServicesImpl();
        int count = placeServices.create(CreateUpdateBase.createUpdatePlace());
        LOG.info(ConstantsView.CREATED + count);
    }
    public static void createRegestryNumber() throws SQLException {

        RegestryNumberServicesImpl regestryNumberServices = new RegestryNumberServicesImpl();
        int count = regestryNumberServices.create(CreateUpdateBase.createUpdareRegestryNumber());
        LOG.info(ConstantsView.CREATED + count);
    }
}
