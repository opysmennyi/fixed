package view_controller;

import view_controller.ConstantView.ConstantsView;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyViewMenu {
    public Map<String, String> menu;

    public MyViewMenu() {
        menu = new LinkedHashMap<>();
        menu.put("A", "   A - Select all table");
        menu.put("B", "   B - Select structure of DB");
        menu.put("1", "   1 - TableAnnotation: " + ConstantsView.AGENCY);
        menu.put("11", "  11 - " + ConstantsView.CREATE + ConstantsView.AGENCY);
        menu.put("12", "  12 - " + ConstantsView.UPDATE + ConstantsView.AGENCY);
        menu.put("13", "  13 - " + ConstantsView.DELETE + ConstantsView.AGENCY);
        menu.put("14", "  14 - " + ConstantsView.SELECT + ConstantsView.AGENCY);
        menu.put("15", "  15 - " + ConstantsView.FINDBYID + ConstantsView.AGENCY);
        menu.put("2", "   2 - TableAnnotation: " + ConstantsView.AGENT);
        menu.put("21", "  21 - " + ConstantsView.CREATE + ConstantsView.AGENT);
        menu.put("22", "  22 - " + ConstantsView.UPDATE + ConstantsView.AGENT);
        menu.put("23", "  23 - " + ConstantsView.DELETE + ConstantsView.AGENT);
        menu.put("24", "  24 - " + ConstantsView.SELECT + ConstantsView.AGENT);
        menu.put("25", "  25 - " + ConstantsView.FINDBYID + ConstantsView.AGENT);
        menu.put("3", "   3 - TableAnnotation: " + ConstantsView.AHA);
        menu.put("31", "  31 - " + ConstantsView.CREATE + ConstantsView.AHA);
        menu.put("32", "  32 - " + ConstantsView.UPDATE + ConstantsView.AHA);
        menu.put("33", "  33 - " + ConstantsView.DELETE + ConstantsView.AHA);
        menu.put("34", "  34 - " + ConstantsView.SELECT + ConstantsView.AHA);
        menu.put("35", "  35 - " + ConstantsView.FINDBYID + ConstantsView.AHA);
        menu.put("4", "   4 - TableAnnotation: " + ConstantsView.AHL);
        menu.put("41", "  41 - " + ConstantsView.CREATE + ConstantsView.AHL);
        menu.put("42", "  42 - " + ConstantsView.UPDATE + ConstantsView.AHL);
        menu.put("43", "  43 - " + ConstantsView.DELETE + ConstantsView.AHL);
        menu.put("44", "  44 - " + ConstantsView.SELECT + ConstantsView.AHL);
        menu.put("45", "  45 - " + ConstantsView.FINDBYID + ConstantsView.AHL);
        menu.put("5", "   5 - TableAnnotation: " + ConstantsView.CLIENT);
        menu.put("51", "  51 - " + ConstantsView.CREATE + ConstantsView.CLIENT);
        menu.put("52", "  52 - " + ConstantsView.UPDATE + ConstantsView.CLIENT);
        menu.put("53", "  53 - " + ConstantsView.DELETE + ConstantsView.CLIENT);
        menu.put("54", "  54 - " + ConstantsView.SELECT + ConstantsView.CLIENT);
        menu.put("55", "  55 - " + ConstantsView.FINDBYID + ConstantsView.CLIENT);
        menu.put("6", "   6 - TableAnnotation: " + ConstantsView.LANDLORD);
        menu.put("61", "  61 - " + ConstantsView.CREATE + ConstantsView.LANDLORD);
        menu.put("62", "  62 - " + ConstantsView.UPDATE + ConstantsView.LANDLORD);
        menu.put("63", "  63 - " + ConstantsView.DELETE + ConstantsView.LANDLORD);
        menu.put("64", "  64 - " + ConstantsView.SELECT + ConstantsView.LANDLORD);
        menu.put("65", "  65 - " + ConstantsView.FINDBYID + ConstantsView.LANDLORD);
        menu.put("7", "   7 - TableAnnotation: " + ConstantsView.PERSONALINFO);
        menu.put("71", "  71 - " + ConstantsView.CREATE + ConstantsView.PERSONALINFO);
        menu.put("72", "  72 - " + ConstantsView.UPDATE + ConstantsView.PERSONALINFO);
        menu.put("73", "  73 - " + ConstantsView.DELETE + ConstantsView.PERSONALINFO);
        menu.put("74", "  74 - " + ConstantsView.SELECT + ConstantsView.PERSONALINFO);
        menu.put("75", "  75 - " + ConstantsView.FINDBYID + ConstantsView.PERSONALINFO);
        menu.put("8", "   8 - TableAnnotation: " + ConstantsView.PLACE);
        menu.put("81", "  81 - " + ConstantsView.CREATE + ConstantsView.PLACE);
        menu.put("82", "  82 - " + ConstantsView.UPDATE + ConstantsView.PLACE);
        menu.put("83", "  83 - " + ConstantsView.DELETE + ConstantsView.PLACE);
        menu.put("84", "  84 - " + ConstantsView.SELECT + ConstantsView.PLACE);
        menu.put("85", "  85 - " + ConstantsView.FINDBYID + ConstantsView.PLACE);
        menu.put("9", "   9 - TableAnnotation: " + ConstantsView.REGESTRYNUMBER);
        menu.put("91", "  91 - " + ConstantsView.CREATE + ConstantsView.REGESTRYNUMBER);
        menu.put("92", "  92 - " + ConstantsView.UPDATE + ConstantsView.REGESTRYNUMBER);
        menu.put("93", "  93 - " + ConstantsView.DELETE + ConstantsView.REGESTRYNUMBER);
        menu.put("94", "  94 - " + ConstantsView.SELECT + ConstantsView.REGESTRYNUMBER);
        menu.put("95", "  95 - " + ConstantsView.FINDBYID + ConstantsView.REGESTRYNUMBER);
        menu.put("W", " W - TableAnnotation: " + "find Agency Agent Client Landlord Info");
        menu.put("E", " E - TableAnnotation: " + "INFO CLIEN/TAGENT");
        menu.put("R", " R - TableAnnotation: " + "Final price of services: ");
        menu.put("Q", "   Q - exit");
    }
}
