package view_controller.finite_methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.persistant_implementation.ConnectionManager;
import view_controller.ConstantView.ConstantsView;
import view_controller.view_submodel.CreateMethods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

public class FinalPrice {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);


   public static Set<Integer> finalizePrice() throws SQLException {
        Set<Integer> price= new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(ConstantsView.FINALPRICE)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info("\nid agency is - " + resultSet.getInt(1)+ "\nid place is - "
                            + resultSet.getInt(2) +"\nfinal price - "
                            + resultSet.getDouble(3));
                    price.add(resultSet.getInt(1));
                }
            }
            return price;
        }
    }

}
