package view_controller.finite_methods;

import dao.constans_dao.ConstantDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.persistant_implementation.ConnectionManager;
import view_controller.ConstantView.ConstantsView;
import view_controller.view_submodel.CreateMethods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

public class AgencyAgentClientLandlordInfo {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);
    public static Set<Integer> findAgencyAgentClientLandorInfo() throws SQLException {
        Set<Integer> findAgencyAgentClientLandorInfo= new LinkedHashSet<>();
        try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(ConstantsView.AACL)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    LOG.info(("\nid_agency - " + resultSet.getInt(1) +
                            "\nname_agency - " + resultSet.getString(2) +
                            "\nid_agent - " + resultSet.getInt(3) +
                            "\npassport_id - " + resultSet.getString(4) +
                            "\nid_client - " + resultSet.getInt(5) +
                            "\npassport_id - " + resultSet.getString(6) +
                            "\nid_landor - " + resultSet.getInt(7) +
                            "\npassport_id - " + resultSet.getString(8)));
                    findAgencyAgentClientLandorInfo.add(resultSet.getInt(1));
                }
            }
            return findAgencyAgentClientLandorInfo;
        }
    }
}
