package view_controller.finite_methods;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import persistant.persistant_implementation.ConnectionManager;
import view_controller.ConstantView.ConstantsView;
import view_controller.view_submodel.CreateMethods;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;


public class AgentClientInfo {
    private static Logger LOG = LogManager.getLogger(CreateMethods.class);

public static Set<Integer> showClientAgentInfo() throws SQLException {
    Set<Integer> ClientAgentInfo= new LinkedHashSet<>();
    try (PreparedStatement preparedStatement = ConnectionManager.getConnection().prepareStatement(ConstantsView.INFOCLIENTAGENT)) {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                LOG.info("\nname - " + resultSet.getString(1) +
                            "\nid gent - " + resultSet.getInt(2) +
                            "\nid client - " + resultSet.getInt(3));
                ClientAgentInfo.add(resultSet.getInt(1));
            }
        }
        return ClientAgentInfo;
    }
}
}
