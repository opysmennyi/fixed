package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "agent_has_agency")
public class Agent_has_agency {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "agent_id_agent")
    private int id_agent;
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "agency_id_agency")
    private int id_agency;

    public Agent_has_agency() {
    }

    public Agent_has_agency(int id_agent, int id_agency) {
        this.id_agent = id_agent;
        this.id_agency = id_agency;
    }

    public int getId_agent() {
        return id_agent;
    }

    public void setId_agent(int id_agent) {
        this.id_agent = id_agent;
    }

    public int getId_agency() {
        return id_agency;
    }

    public void setId_agency(int id_agency) {
        this.id_agency = id_agency;
    }

    @Override
    public String toString() {
        return String.format("%d %d", id_agent, id_agency);
    }
}
