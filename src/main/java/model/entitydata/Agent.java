package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "agent")
public class Agent {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "id_agent")
    private int id_agent;
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "personal_info_passport_id")
    private String personal_info_passport_id;

    public Agent() {
    }

    public Agent(int id_agent, String personal_info_passport_id) {
        this.id_agent = id_agent;
        this.personal_info_passport_id = personal_info_passport_id;
    }

    public int getId_agent() {
        return id_agent;
    }

    public void setId_agent(int id_agent) {
        this.id_agent = id_agent;
    }

    public String getPersonal_info_passport_id() {
        return personal_info_passport_id;
    }

    public void setPersonal_info_passport_id(String personal_info_passport_id) {
        this.personal_info_passport_id = personal_info_passport_id;
    }

    @Override
    public String toString() {
        return String.format("%d %-11s", id_agent, personal_info_passport_id);
    }
}
