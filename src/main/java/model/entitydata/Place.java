package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "place")
public class Place {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "id_place")
    private int id_place;
    @ColumnAnnotation(name = "square")
    private int square;
    @ColumnAnnotation(name = "price")
    private double price;
    @ColumnAnnotation(name = "location")
    private String location;
    @ColumnAnnotation(name = "type")
    private String type;
    @ColumnAnnotation(name = "landlord_id_landor")
    private int landor_id;
    @ColumnAnnotation(name = "regestry_number_regestry_number")
    private String regestry_number;

    public Place() {
    }

    public Place(int id_place, int square, double price, String location, String type, int landor_id, String regestry_number) {
        this.id_place = id_place;
        this.square = square;
        this.price = price;
        this.location = location;
        this.type = type;
        this.landor_id = landor_id;
        this.regestry_number = regestry_number;
    }

    public int getId_place() {
        return id_place;
    }

    public void setId_place(int id_place) {
        this.id_place = id_place;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLandor_id() {
        return landor_id;
    }

    public void setLandor_id(int landor_id) {
        this.landor_id = landor_id;
    }

    public String getRegestry_number() {
        return regestry_number;
    }

    public void setRegestry_number(String regestry_number) {
        this.regestry_number = regestry_number;
    }

    @Override
    public String toString() {
        return String.format("%d %d %f %-11s %-11s %d %-11s",
                id_place,
                square,
                price,
                location,
                type,
                landor_id,
                regestry_number);
    }
}
