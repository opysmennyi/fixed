package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "landor")
public class Landlord {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "id_landor")
    private int id_landor;
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "personal_info_passport_id")
    private String personal_info_passport_id;

    public Landlord() {
    }

    public Landlord(int id_landor, String personal_info_passport_id) {
        this.id_landor = id_landor;
        this.personal_info_passport_id = personal_info_passport_id;
    }

    public int getId_landor() {
        return id_landor;
    }

    public void setId_landor(int id_landor) {
        this.id_landor = id_landor;
    }

    public String getPersonal_info_passport_id() {
        return personal_info_passport_id;
    }

    public void setPersonal_info_passport_id(String personal_info_passport_id) {
        this.personal_info_passport_id = personal_info_passport_id;
    }

    @Override
    public String toString() {
        return String.format("%d %-11s", id_landor, personal_info_passport_id);
    }
}
