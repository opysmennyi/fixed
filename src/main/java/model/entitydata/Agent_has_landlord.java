package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "agent_has_landlord")
public class Agent_has_landlord {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "agent_id_agent")
    private int id_agent;
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "landlord_id_landor")
    private int id_landor;

    public Agent_has_landlord() {
    }

    public Agent_has_landlord(int id_agent, int id_landor) {
        this.id_agent = id_agent;
        this.id_landor = id_landor;
    }

    public int getId_agent() {
        return id_agent;
    }

    public void setId_agent(int id_agent) {
        this.id_agent = id_agent;
    }

    public int getId_landor() {
        return id_landor;
    }

    public void setId_landor(int id_landor) {
        this.id_landor = id_landor;
    }

    @Override
    public String toString() {
        return String.format("%d %d", id_agent, id_landor);
    }
}
