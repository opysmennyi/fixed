package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "personal_info")
public class Personal_info {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "passport_id")
    private String passport_id;
    @ColumnAnnotation(name = "name")
    private String name;
    @ColumnAnnotation(name = "surname")
    private String surname;
    @ColumnAnnotation(name = "age")
    private int age;
    @ColumnAnnotation(name = "family_status")
    private String family_status;

    public Personal_info() {
    }

    public Personal_info(String passport_id, String name, String surname, int age, String family_status) {
        this.passport_id = passport_id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.family_status = family_status;
    }

    public String getPassport_id() {
        return passport_id;
    }

    public void setPassport_id(String passport_id) {
        this.passport_id = passport_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFamily_status() {
        return family_status;
    }

    public void setFamily_status(String family_status) {
        this.family_status = family_status;
    }

    @Override
    public String toString() {
        return String.format("%-11s %-11s %-11s %d %-11s",
                passport_id,
                name,
                surname,
                age,
                family_status);
    }
}
