package model.entitydata;

import model.annotation.ColumnAnnotation;
import model.annotation.PrimaryKeyAnnotation;
import model.annotation.TableAnnotation;

@TableAnnotation(name = "agency")
public class Agency {
    @PrimaryKeyAnnotation
    @ColumnAnnotation(name = "id_agency")
    private int id_agency;
    @ColumnAnnotation(name = "name_agency")
    private String name_agency;
    @ColumnAnnotation(name = "city")
    private String city;
    @ColumnAnnotation(name = "payment_terms")
    private String payment_terms;
    @ColumnAnnotation(name = "markup")
    private double markup;
    @ColumnAnnotation(name = "agency_services_price")
    private double agency_services_price;
    @ColumnAnnotation(name = "agreemen_termination")
    private String agreement_termination;

    public Agency() {
    }

    public Agency(int id_agency, String name_agency, String city, String payment_terms, double markup, double agency_services_price, String agreement_termination) {
        this.id_agency = id_agency;
        this.name_agency = name_agency;
        this.city = city;
        this.payment_terms = payment_terms;
        this.markup = markup;
        this.agency_services_price = agency_services_price;
        this.agreement_termination = agreement_termination;
    }

    public int getId_agency() {
        return id_agency;
    }

    public void setId_agency(int id_agency) {
        this.id_agency = id_agency;
    }

    public String getName_agency() {
        return name_agency;
    }

    public void setName_agency(String name_agency) {
        this.name_agency = name_agency;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPayment_terms() {
        return payment_terms;
    }

    public void setPayment_terms(String payment_terms) {
        this.payment_terms = payment_terms;
    }

    public double getMarkup() {
        return markup;
    }

    public void setMarkup(double markup) {
        this.markup = markup;
    }

    public double getAgency_services_price() {
        return resPrice();
    }

    public void setAgency_services_price(double agency_services_price) {
        this.agency_services_price = agency_services_price;
    }

    public String getAgreement_termination() {
        return agreement_termination;
    }

    public void setAgreement_termination(String agreement_termination) {
        this.agreement_termination = agreement_termination;
    }

    @Override
    public String toString() {
        return String.format("%d %-15s %-15s %-15s %-15f %-15f %-15s", id_agency, name_agency, city, payment_terms, markup, getAgency_services_price(), agreement_termination);
    }

    private double resPrice() {
        Place place = new Place();
        double res = agency_services_price * place.getPrice();
        return res;
    }
}
