package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.AgentHasLandorDAO;
import model.entitydata.Agent_has_landlord;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AgentHasLandorDaoImpl implements AgentHasLandorDAO {
    @Override
    public List<Agent_has_landlord> findAll() throws SQLException {
        List<Agent_has_landlord> agent_has_landlords = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_AHL)) {
                while (resultSet.next()) {
                    agent_has_landlords.add((Agent_has_landlord) new Transformer(Agent_has_landlord.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return agent_has_landlords;
    }

    @Override
    public Agent_has_landlord findById(Integer id_agent) throws SQLException {
        Agent_has_landlord agent_has_landlord = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_AHL)) {
            preparedStatement.setInt(1, id_agent);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    agent_has_landlord = (Agent_has_landlord) new Transformer(AgentHasLandorDAO.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return agent_has_landlord;
    }

    @Override
    public int create(Agent_has_landlord agent_has_landlord) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_AHL)) {
            preparedStatement.setInt(1, agent_has_landlord.getId_agent());
            preparedStatement.setInt(2, agent_has_landlord.getId_landor());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Agent_has_landlord agent_has_landlord) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_AHL)) {
            preparedStatement.setInt(1, agent_has_landlord.getId_agent());
            preparedStatement.setInt(2, agent_has_landlord.getId_landor());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Agent_has_landlord id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_AHL)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
