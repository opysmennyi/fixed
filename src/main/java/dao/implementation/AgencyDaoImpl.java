package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.AgencyDAO;
import model.entitydata.Agency;
import persistant.persistant_implementation.ConnectionManager;
import transformer.Transformer;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("Duplicates")
public class AgencyDaoImpl implements AgencyDAO {


    @Override
    public List<Agency> findAll() throws SQLException {
        List<Agency> agencies = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_AGENCIES)) {
                while (resultSet.next()) {
                    agencies.add((Agency) new Transformer(Agency.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return agencies;
    }

    @Override
    public Agency findById(Integer id_agency) throws SQLException {
        Agency agency = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_AGENCY)) {
            preparedStatement.setInt(1, id_agency);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    agency = (Agency) new Transformer(Agency.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return agency;

    }

    @Override
    public int create(Agency agency) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_AGENCY)) {
            preparedStatement.setInt(1, agency.getId_agency());
            preparedStatement.setString(2, agency.getName_agency());
            preparedStatement.setString(3, agency.getCity());
            preparedStatement.setString(4, agency.getPayment_terms());
            preparedStatement.setDouble(5, agency.getMarkup());
            preparedStatement.setDouble(6, agency.getAgency_services_price());
            preparedStatement.setString(7, agency.getAgreement_termination());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Agency agency) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_AGENCY)) {
            preparedStatement.setInt(1, agency.getId_agency());
            preparedStatement.setString(2, agency.getName_agency());
            preparedStatement.setString(3, agency.getCity());
            preparedStatement.setString(4, agency.getPayment_terms());
            preparedStatement.setDouble(5, agency.getMarkup());
            preparedStatement.setDouble(6, agency.getAgency_services_price());
            preparedStatement.setString(7, agency.getAgreement_termination());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Agency id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_AGENCY)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
