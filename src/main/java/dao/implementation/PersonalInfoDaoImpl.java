package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.PersonalInfoDAO;
import model.entitydata.Personal_info;
import transformer.Transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class PersonalInfoDaoImpl implements PersonalInfoDAO {
    @Override
    public List<Personal_info> findAll() throws SQLException {
        List<Personal_info> personal_infos = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_PERSONALINFO)) {
                while (resultSet.next()) {
                    personal_infos.add((Personal_info) new Transformer(Personal_info.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return personal_infos;
    }

    @Override
    public Personal_info findById(Integer passport_id) throws SQLException {
        Personal_info personal_info = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_PERSONALINFO)) {
            preparedStatement.setInt(1, passport_id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    personal_info = (Personal_info) new Transformer(Personal_info.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return personal_info;
    }

    @Override
    public int create(Personal_info passport_id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_PERSONALINFO)) {
            preparedStatement.setString(1, passport_id.getPassport_id());
            preparedStatement.setString(2, passport_id.getName());
            preparedStatement.setString(3, passport_id.getSurname());
            preparedStatement.setInt(4, passport_id.getAge());
            preparedStatement.setString(5, passport_id.getFamily_status());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Personal_info passport_id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_PERSONALINFO)) {
            preparedStatement.setString(1, passport_id.getPassport_id());
            preparedStatement.setString(2, passport_id.getName());
            preparedStatement.setString(3, passport_id.getSurname());
            preparedStatement.setInt(4, passport_id.getAge());
            preparedStatement.setString(5, passport_id.getFamily_status());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Personal_info id) throws SQLException {
        return 0;
    }


    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_PERSONALINFO)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
