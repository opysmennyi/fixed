package dao.implementation;

import dao.constans_dao.ConstantDAO;
import dao.interfaces.AgentDAO;
import model.entitydata.Agent;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AgentDaoImpl implements AgentDAO {
    @Override
    public List<Agent> findAll() throws SQLException {
        List<Agent> agents = new ArrayList<>();
        try (Statement statement = ConstantDAO.CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(ConstantDAO.FIND_ALL_AGENT)) {
                while (resultSet.next()) {
                    agents.add((Agent) new Transformer(Agent.class).fromResultSetToAgency(resultSet));
                }
            }
        }
        return agents;
    }

    @Override
    public Agent findById(Integer id_agent) throws SQLException {
        Agent agent = null;
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.FIND_BY_ID_AGENT)) {
            preparedStatement.setInt(1, id_agent);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    agent = (Agent) new Transformer(Agent.class).fromResultSetToAgency(resultSet);
                    break;
                }
            }
        }
        return agent;
    }

    @Override
    public int create(Agent agent) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.CREATE_AGENT)) {
            preparedStatement.setInt(1, agent.getId_agent());
            preparedStatement.setString(2, agent.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Agent agent) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.UPDATE_AGENT)) {
            preparedStatement.setInt(1, agent.getId_agent());
            preparedStatement.setString(2, agent.getPersonal_info_passport_id());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Agent id) throws SQLException {
        return 0;
    }

    @Override
    public int delete(Integer id) throws SQLException {
        try (PreparedStatement preparedStatement = ConstantDAO.CONNECTION.prepareStatement(ConstantDAO.DELETE_AGENT)) {
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        }
    }
}
