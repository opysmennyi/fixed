package dao.interfaces;

import model.entitydata.RegestryNumber;

import java.sql.SQLException;

public interface RegestryNumberDAO extends GeneralDAO<RegestryNumber, Integer> {
    int delete(Integer regestryNumber) throws SQLException;
}
